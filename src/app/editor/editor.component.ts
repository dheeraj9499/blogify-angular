import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Blog, IBlog } from '../shared/models/blog';
import { EditorService } from './editor.service';

@Component({
  selector: 'bp-editor',
  templateUrl: './editor.component.html',
  styleUrls: ['./editor.component.scss'],
})
export class EditorComponent implements OnInit {

  public blog: FormGroup;
  public preview: boolean = false;
  private blogId: string = '-1';
  public blogEdit: boolean = false;
  errorText: string;
  isError: boolean = false;

  constructor(
    private fb: FormBuilder,
    private editorService: EditorService,
    private activatedRoutes: ActivatedRoute,
    private router: Router
  ) {
    this.blog = this.fb.group({
      // userId: ['', Validators.required, Validators.minLength()],
      title: ['', [Validators.required, Validators.minLength(5)]],
      tags: ['', [Validators.required, Validators.minLength(5)]],
      body: ['', [Validators.required, Validators.minLength(30)]],
    });
  }

  ngOnInit(): void {
    this.activatedRoutes.params.subscribe((param) => {
      if (param.id) {
        this.blogId = param.id;
      }
    });
    if (this.blogId !== '-1') {
      this.getBlogForEdit(this.blogId);
      this.blogEdit = true;
    }
  }

  get blogData() {
    return this.blog.controls;
  }

  /**
   * @Posts blog
   * @returns blog data
   */
  public postBlog(): void {
    // if invalid, do nothing
    if (this.blog.invalid) {
      return;
    }
    // grabs data
    const data = {
      title: this.blog.value.title,
      tags: this.blog.value.tags,
      body: this.blog.value.body,
      hasPublished: true
    };
    const blog: Blog = new Blog(data);
    this.editorService.postBlog(blog).subscribe(
      (res) => {
        this.router.navigate(['/u/myblogs']);
      },
      (error) => {
        this.isError = true;
        this.errorText = error;
      }
    );
  }

  /**
   * Gets blog for edit
   * @param blogId
   */
  getBlogForEdit(blogId: string): void {
    this.editorService.getEditBlog(blogId)
      .subscribe((result) => {
        if (result) {
          this.blog.controls.title.setValue(result.title);
          this.blog.controls.tags.setValue(result.tags);
          this.blog.controls.body.setValue(result.body);
        }
    });
  }

  public postEditedBlog(): void {
    // if invalid, do nothing
    if (this.blog.invalid) {
      return;
    }
    // grabs data
    const data: IBlog = {
      _id: this.blogId,
      title: this.blog.value.title,
      tags: this.blog.value.tags,
      body: this.blog.value.body,
      hasPublished: true
    };
    this.editorService.postEditedBlog(data).subscribe(
      () => {
        this.router.navigate(['/u/myblogs']);
      },
      (err) => {
        console.log(err);
      }
    );
  }
}
