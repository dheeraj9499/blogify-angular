import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EditorRoutingModule } from './editor-routing.module';
import { EditorComponent } from './editor.component';
import { ReactiveFormsModule } from '@angular/forms';
import { EditorService } from './editor.service';
import { AuthGuard } from '../shared/guards/auth.guard';


@NgModule({
  declarations: [EditorComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    EditorRoutingModule
  ],
  providers: [EditorService, AuthGuard],
  exports: [EditorComponent]
})
export class EditorModule { }
