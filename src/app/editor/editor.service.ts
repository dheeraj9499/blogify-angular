import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Blog, IBlog } from '../shared/models/blog';
import { SERVER_ENDPOINT } from '../shared/constants';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EditorService {

  constructor(private http: HttpClient) { };

  /***
   * @param blog carries Blog type data { userId, title, tags, body }
   */
  postBlog(blog: Blog): Observable<any> {
    return this.http.post<any>(`${SERVER_ENDPOINT}/editor/new/post`, blog);
  }

  /**
   * Edits blog
   * @param blogId
   * @returns blog of type IBlog
   */
  getEditBlog(blogId: string): Observable<IBlog> {
    return this.http.get<IBlog>(`${SERVER_ENDPOINT}/u/blog/edit/${blogId}`);
  }
  postEditedBlog(blog: IBlog): Observable<any> {
    return this.http.post<any>(`${SERVER_ENDPOINT}/u/blog/edit/`, blog);
  }
}
