import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './core/home/home.component';
import { LoginAccessGuard } from './shared/guards/login-access.guard';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
    pathMatch: 'full'
  },
  {
    path: 'auth',
    canActivate: [LoginAccessGuard],
    loadChildren: () => import('./auth/auth.module')
                          .then(module => module.AuthModule)
  },
  {
    path: 'editor',
    loadChildren: () => import('./editor/editor.module')
                          .then(module => module.EditorModule)
  },
  {
    path: 'u',
    loadChildren: () => import('./user/user.module').then(module => module.UserModule)
  },
  {
    path: 'blog',
    loadChildren: () => import('./blog/blog.module').then(module => module.BlogModule)
  },
  {
    path: '**',
    redirectTo: ''
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
