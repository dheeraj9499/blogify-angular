export class User {
  username: string;
  email: string;
  password: string;
  constructor(userData: any) {
    this.username = userData.username;
    this.email = userData.email;
    this.password = userData.password;
  }
}
