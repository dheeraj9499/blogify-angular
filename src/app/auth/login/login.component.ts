import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';
import { User } from '../models/user';

@Component({
  selector: 'bp-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  @Output() authUIChange: EventEmitter<boolean> = new EventEmitter<boolean>();

  loginForm: FormGroup;
  errorText: string;
  isError: boolean = false;
  emptyField: boolean = false;

  constructor(private fb: FormBuilder, private authService: AuthService, private router: Router) {
    this.loginForm = this.fb.group({
      email: [
        '',
        [
          Validators.required,
          Validators.minLength(10),
          Validators.maxLength(40),
          Validators.email,
        ],
      ],
      password: [
        '',
        [
          Validators.required,
          Validators.minLength(8),
          Validators.maxLength(50),
          Validators.pattern(
            '^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$'
          ),
        ],
      ],
    });
  }
  ngOnInit(): void {}

  /**
   * @getter form
   */
  get form() {
    return this.loginForm.controls;
  }

  /**
   * Logins validate and save
   * @returns validate and save
   */
  loginValidateAndAccess(): void {
    if (this.loginForm.invalid) {
      this.emptyField = true;
      return;
    }
    const user: User = new User(this.loginForm.value);
    this.authService.login(user).subscribe(
      (res) => {
        localStorage.setItem('access-token', res.token);
        localStorage.setItem('username', res.username);
        this.router.navigate(['/']);
      },
      (error) => {
        this.isError = true;
        this.errorText = error;
      }
    );
  }
  showSignup(): void {
    this.authUIChange.emit(false);
  }
}
