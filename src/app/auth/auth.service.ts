import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from './models/user';
import { SERVER_ENDPOINT } from '../shared/constants';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';


@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient, private router: Router) { }

  /**
   * @param user
   * @returns Observable of type any contains response from the
  */
  public signUp(user: User): Observable<any> {
    return this.http.post<any>(`${SERVER_ENDPOINT}/auth/signup`, user);
  }


  /**
   * @param user
   * @returns Observable of type any contains response from the server
   */
  public login(user: User): Observable<any> {
    return this.http.post<any>(`${SERVER_ENDPOINT}/auth/login`, user);
  }

  /**
   * Determines whether user logged in
   * @returns true if user logged in else false
   */
  public isUserLoggedIn(): boolean {
    return !!localStorage.getItem('access-token');
  }

  /**
   * Logs out
   */
  public logOut(): void {
    localStorage.removeItem('access-token');
    this.router.navigate(['/']);
    return;
  }

  public getMyId(): Observable<string> {
    return this.http.get<string>(`${SERVER_ENDPOINT}/auth/getMyId`);
  }
  /**
   * @returns token
   */
  get getToken() {
    return localStorage.getItem('access-token');
  }
}
