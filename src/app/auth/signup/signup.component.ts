import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { boolean } from 'joi';
import { AuthService } from '../auth.service';
import { User } from '../models/user';

@Component({
  selector: 'bp-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss'],
})
export class SignupComponent implements OnInit {

  @Output() authUIChange: EventEmitter<boolean> = new EventEmitter<boolean>();
  public signUpForm: FormGroup;
  errorText: string;
  isError: boolean = false;
  emptyField: boolean = false;

  constructor(private fb: FormBuilder, private authService: AuthService, private router: Router) {
    this.signUpForm = this.fb.group({
      username: [
        '',
        [
          Validators.required,
          Validators.minLength(3),
          Validators.maxLength(25),
        ],
      ],
      email: [
        '',
        [
          Validators.required,
          Validators.minLength(10),
          Validators.maxLength(40),
          Validators.email,
        ],
      ],
      password: [
        '',
        [
          Validators.required,
          Validators.minLength(8),
          Validators.maxLength(50),
          Validators.pattern(
            '^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$'
          ),
        ],
      ],
    });
  }
  ngOnInit(): void {}

  /**
   * @getter form
   */
  get form() {
    return this.signUpForm.controls;
  }

  /**
   * Shows login
   */
  public showLogin(): void {
    this.authUIChange.emit(true);
  }

  /**
   * Signups validate and save
   * @returns validate and save the signup form data
   */
  public signupValidateAndSave(): void {
    if (this.signUpForm.invalid) {
      this.emptyField = true;
      return;
    }
    const user: User = new User(this.signUpForm.value);
    this.authService.signUp(user).subscribe(
      (res) => {
        localStorage.setItem('access-token', res.token);
        localStorage.setItem('username', res.username);
        this.router.navigate(['/']);
      },
      (error) => {
        this.isError = true;
        this.errorText = error;
      }
    );
  }
}
