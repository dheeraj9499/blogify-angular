import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'bp-auth',
  template: `
    <div class="container transition">
      <div [hidden]="isHide">
        <bp-login (authUIChange)="changeAuthUI($event)"></bp-login>
      </div>
      <div [hidden]="!isHide">
        <bp-signup (authUIChange)="changeAuthUI($event)"></bp-signup>
      </div>
    </div>
  `,
  styleUrls: ['./auth.component.scss'],
})
export class AuthComponent implements OnInit {
  isHide: boolean = false;

  constructor() {}

  ngOnInit(): void {}

  changeAuthUI = (event: any): void => {
    this.isHide = !event;
  };
}
