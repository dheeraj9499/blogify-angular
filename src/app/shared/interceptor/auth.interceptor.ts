import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { AuthService } from 'src/app/auth/auth.service';
import { catchError, finalize, timeInterval } from 'rxjs/operators';
import { LoaderService } from '../services/loader.service';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  constructor(private authService: AuthService, public loaderService: LoaderService) {}

  intercept(req: HttpRequest<any>,next: HttpHandler): Observable<HttpEvent<any>> {
    this.loaderService.isLoading$.next(true);
    const headers = req.headers
      .set('Content-Type', 'application/json')
      .set('Authorization', `Bearer ${this.authService.getToken}`);
    const authReq = req.clone({ headers });
    return next.handle(authReq)
    .pipe(
      catchError((error: HttpErrorResponse) => {
        let errorMsg = '';
        if (error instanceof HttpErrorResponse) {
          // Server side error
          errorMsg = error.error.message;

        } else {
          // Client side error
          errorMsg = `Error: ${error},  Message: ${error}`;
        }
        return throwError(errorMsg);
      }),
      finalize(
        () => {
          this.loaderService.isLoading$.next(false);
        }
      )
    )
  }
}
