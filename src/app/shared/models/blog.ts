export interface IBlog {
  _id?: string;
  userId?: {
    _id: string;
    username: string;
  };
  title: string;
  tags: string;
  body?: string;
  hasPublished: boolean;
  createdAt?: string;
  meta?: {
    like?: number,
    likedBy: string[]
  }
}
export class Blog implements IBlog {
  title: string;
  tags: string;
  body: string;
  hasPublished: boolean = false;
  meta: {
    like: number,
    likedBy: string[]
  };
  constructor(blog: any) {
    this.title = blog.title;
    this.tags = blog.tags;
    this.body = blog.body;
    this.hasPublished = blog.hasPublished;
    this.meta = {
      like: 0, likedBy: []
    };
  }
}
