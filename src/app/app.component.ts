import { AfterContentChecked, ChangeDetectorRef, Component } from '@angular/core';
import { LoaderService } from './shared/services/loader.service';

@Component({
  selector: 'bp-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements AfterContentChecked{
  title = 'Blogify';
  constructor(public loaderService: LoaderService, private cd: ChangeDetectorRef) { }

  // to overcome ExpressionChangedAfterItHasBeenCheckedError
  ngAfterContentChecked() {
    this.cd.detectChanges();
  }
}
