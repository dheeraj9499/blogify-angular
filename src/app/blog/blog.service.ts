import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { SERVER_ENDPOINT } from '../shared/constants';
import { IBlog } from '../shared/models/blog';

@Injectable({
  providedIn: 'root'
})
export class BlogService {

  constructor(private http: HttpClient) { }

  /**
   * Getblogs by id
   * @returns Observable of blog object
   */
  public getBlogById(blogId: string): Observable<IBlog> {
    return this.http.get<IBlog>(`${SERVER_ENDPOINT}/blog/${blogId}`);
  }

  public postLike(blogId: string): Observable<any> {
    return this.http.post<any>(`${SERVER_ENDPOINT}/blog/like`, { blogId });
  }
}
