import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { IBlog } from 'src/app/shared/models/blog';
import { BlogService } from '../blog.service';
import { Observable, of } from 'rxjs';
import { AuthService } from 'src/app/auth/auth.service';
@Component({
  selector: 'bp-blog',
  templateUrl: './blog.component.html',
  styleUrls: ['./blog.component.scss']
})
export class BlogComponent implements OnInit {
  myId: string;
  private blogId: undefined = undefined;
  public data$: Observable<IBlog>;
  public fill: string = '-outline';

  constructor(private blogService: BlogService, private activateRoute: ActivatedRoute, private authService: AuthService, private router: Router) { }

  ngOnInit(): void {
    this.activateRoute.params.subscribe(
      (param) => {
        this.blogId = param.id;
        this.getBlogById(this.blogId);
      }
    );
    this.authService.getMyId().subscribe(
      (res) => {
        this.myId = res
      },
      (err) => {
        console.log('Watch Mode');
      }
    );
  }

  /**
   * Gets blog tags
   * @returns Obserable of string array
   */
  get getTags(): Observable<string[]> {
    let tags: string[];
    this.data$.subscribe(
      (result) => {
        tags = result.tags.split(', ');
      }
    )
    return of(tags);
  }

  /**
   * Gets blog daa by id
   * @param blogId
   */
  getBlogById(blogId: string): void {
    this.blogService.getBlogById(blogId)
      .subscribe(
        (result) => {
          if (result.meta.likedBy.includes(this.myId)) {
            this.fill = '';
          }
          return this.data$ = of(result);
        },
        (err) => console.error(err)
      );
  }

  /**
   * blog like feature
   * @param blogId
   */
  like(blogId: string): void {
    if (!this.authService.isUserLoggedIn()) {
      this.router.navigate(['/auth']);
      return;
    }
    this.blogService.postLike(blogId)
      .subscribe(
        () => {
          this.getBlogById(blogId);
        },
        (err) => {
          console.error(err);
      });
  }
}
