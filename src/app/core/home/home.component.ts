import { Component, OnInit } from '@angular/core';
import { Observable, of } from 'rxjs';
import { IBlog } from 'src/app/shared/models/blog';
import { HomeService } from './home.service';

@Component({
  selector: 'bp-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  data$: Observable<IBlog[]>;
  errorText: string;
  isError: boolean = false;

  constructor(private homeService: HomeService) {}

  ngOnInit(): void {
    this.getBlogs();
  }

  getTags(tags: string): Observable<string[]> {
    return of(tags.split(','));
  }
  /**
   * Gets published blogs
   */
  getBlogs(): void {
    this.homeService.getAllBlogs().subscribe(
      (result) => {
        this.data$ = of(result);
      },
      (error) => {
        this.isError = true;
        this.errorText = error;
      }
    );
  }
}
