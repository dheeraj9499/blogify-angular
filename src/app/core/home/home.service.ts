import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { SERVER_ENDPOINT } from 'src/app/shared/constants';
import { IBlog } from 'src/app/shared/models/blog';

@Injectable({
  providedIn: 'root',
})
export class HomeService {
  constructor(private http: HttpClient) {}

  getAllBlogs(): Observable<IBlog[]> {
    return this.http.get<IBlog[]>(`${SERVER_ENDPOINT}/home/blogs`);
  }
}
