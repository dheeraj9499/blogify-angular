import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'bp-loading',
  template: `
    <div style="background: transparent;" class="loading">
    </div>
  `,
  styleUrls: ['./loading.component.scss']
})
export class LoadingComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
