import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/auth/auth.service';

@Component({
  selector: 'bp-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss']
})
export class NavComponent implements OnInit {

  constructor(private router: Router, private authService: AuthService) { }

  ngOnInit(): void {
  }

  /**
   * Navigates to home
   */
  navigateToHome(): void {
    this.router.navigateByUrl('/');
  }

  /**
   * Gets boolean whether user logged in
   */
  get userLoggedIn(): boolean {
    return this.authService.isUserLoggedIn();
  }

  /**
   * Logs out
   */
  logOut(): void {
    this.authService.logOut();
  }

  /**
   * Navigates to editor
   */
  navigateToEditor(): void {
    this.router.navigateByUrl('/editor');
  }

  get getUsername() {
    return localStorage.getItem("username");
  }

  /**
   * Navigates to authentication component
   */
  navigateToAuth(): void {
    this.router.navigateByUrl('/auth');
  }
}
