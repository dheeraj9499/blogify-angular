import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UserRoutingModule } from './user-routing.module';
import { MyblogsComponent } from './myblogs/myblogs.component';
import { UserService } from './user.service';
import { EditorModule } from '../editor/editor.module';


@NgModule({
  declarations: [MyblogsComponent],
  imports: [
    CommonModule,
    UserRoutingModule,
    EditorModule
  ],
  providers: [UserService],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class UserModule { }
