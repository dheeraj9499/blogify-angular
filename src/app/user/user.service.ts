import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { SERVER_ENDPOINT } from '../shared/constants';
import { IBlog } from '../shared/models/blog';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }

  /**
   * Gets my blogs
   * @returns my blogs, using bearer token using interceptor
   */
  getMyBlogs(): Observable<IBlog[]> {
    return this.http.get<IBlog[]>(`${SERVER_ENDPOINT}/u/myblogs`);
  }

  /**
   * Deletes my blog
   * @param blogId
   * @returns SUCCESS (200) | FAIL (401)
   */
  deleteMyBlog(blogId: string): Observable<any> {
    return this.http.delete<any>(`${SERVER_ENDPOINT}/u/blog/delete/${blogId}`);
  }
}
