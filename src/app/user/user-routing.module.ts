import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EditorComponent } from '../editor/editor.component';
import { AuthGuard } from '../shared/guards/auth.guard';
import { MyblogsComponent } from './myblogs/myblogs.component';
const routes: Routes = [
  {
    path: 'myblogs',
    component: MyblogsComponent,
    pathMatch: 'full',
    canActivate: [AuthGuard]
  },
  {
    path: 'blog/edit/:id',
    component: EditorComponent,
    pathMatch: 'full',
    canActivate: [AuthGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UserRoutingModule {}
