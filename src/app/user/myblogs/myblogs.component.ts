import { Component, CUSTOM_ELEMENTS_SCHEMA, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, of } from 'rxjs';
import { IBlog } from 'src/app/shared/models/blog';
import { UserService } from '../user.service';

@Component({
  selector: 'bp-myblogs',
  templateUrl: './myblogs.component.html',
  styleUrls: ['./myblogs.component.scss']
})
export class MyblogsComponent implements OnInit {

  data$: Observable<IBlog[]>;
  show = false;
  constructor(private userService: UserService, private router: Router) {}

  ngOnInit(): void {
    this.getMyBlogs();
  }

  /**
   * Gets my blogs
   */
  getMyBlogs(): void {
    this.userService.getMyBlogs()
      .subscribe(
        (result) => {
          if (result.length === 0) {
            this.show = true;
          }
          this.data$ = of(result);
      },
    );
  }

  getTags(tags: string): Observable<string[]> {
    return of(tags.split(','));
  }

  deleteBlog(blogId: string): void {
    const hasToBedelete = confirm('Are you sure you want to delete this blog?');
    if (hasToBedelete) {
      this.userService.deleteMyBlog(blogId)
        .subscribe(
          (res) => {
            this.getMyBlogs();
          }
        );
    }
    return;
  }
}
