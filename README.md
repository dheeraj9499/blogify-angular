# BlogPost Web App

It is a Blog post web app in which user can write their blogs (ofc with crud operation), consisting of authentication with jwt, an editor, blog like feature and more.

## Features

- WYSIWYG Editor alongside previewer
- Authentication/Authorization using JWT
- Blog CRUD
- Blog like feature
- lazy loaded modules, router guards, http interceptors
- etc

## Tech

- [Angular] - The modern web developer's platform
- [SCSS] - Stylesheets
- [node.js] - evented I/O for the backend
- [Express] - fast node.js network app framework
- [JWT] - JWT.IO allows you to decode, verify and generate JWT (Authentication/Authorization).
- [Mongodb] - MongoDB is a general purpose, document-based, distributed database built for modern application developers and for the cloud (use mongodb atlas, if possible)
- [bcrypt] - password hashing

## Installation

It requires [Node.js](https://nodejs.org/) and [Angular](https://angular.io/)
After install nodejs and angular run the following commands in the bash
```sh
mkdir blogpost
cd blogpost
git clone https://gitlab.com/dheeraj9499/blogify-angular.git
```
or you can clone the project from https://gitlab.com/dheeraj9499/blogify-angular.git

1. make a **.env** file in the project root directory
2. paste follwing lines to the **.env** file
3. replace the variable accordingly.

Install the dependencies and devDependencies and start the server.

```sh
PORT = <PORT-NO>
DB_URI = <MONGODB_CONNECTION_STRING>
DATABASE_NAME = <DATABASE_NAME>
SECRET = <TOKEN_SECRET_STRING>
```
then finally 😅
```sh
npm install
```
open two bash/terminal tabs and run the commands
```sh
npm start
npm run server
```
👍👍👍

(If nodemon error occurs on npm start plese run folowing command)
```sh
npm i -g nodemon
```
[Angular]: <https://angular.io/>
[node.js]: <http://nodejs.org>
[express]: <http://expressjs.com>
[EJS]: <https://ejs.co/>
[SCSS]: <https://sass-lang.com/>
[MongoDB]: <https://www.mongodb.com//>
[JWT]: <https://jwt.io/>
[bcrypt]: <https://www.npmjs.com/package/bcrypt>

