const express = require('express');
const cors = require('cors');
const morgan = require('morgan');
const cookieParser = require('cookie-parser');
require('dotenv').config();
const { dbConnect } = require('./config/dbConfig');
const { logger } = require('./logger/logger');
const app = express();
app.use(express.json());
app.use(express.urlencoded({
  extended: true
}));
app.use(cors());
app.use(morgan('combined', { stream: logger }));

// Todo
app.use(cookieParser());

const homeRoutes = require('./routes/homeRoutes');
const blogRoutes = require('./routes/blogRoutes');
const authRoutes = require('./routes/authRoutes');
const userRoutes = require('./routes/userRoutes');
const editorRoutes = require('./routes/editorRoutes');

console.log(`In ${process.env.NODE_ENV}`);
(() => dbConnect(app))();

app.use('/home', homeRoutes);
app.use('/auth', authRoutes);
app.use('/blog', blogRoutes);
app.use('/u', userRoutes);
app.use('/editor', editorRoutes);
