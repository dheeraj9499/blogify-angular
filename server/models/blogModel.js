const mongoose = require('mongoose');
const { Schema } = mongoose;

const blogSchema = new Schema({
  userId: {
    type: String,
    required: true,
    ref: 'user'
  },
  title: {
    type: String,
    trim: true,
    min: 5,
    max: 50,
    required: true
  },
  body: {
    type: String,
    trim: true,
    min: 6,
    max: 2048,
    required: true
  },
  tags: {
    type: String,
    minLength: 6,
    maxLength: 100,
    required: true
  },
  hasPublished: {
    type: Boolean,
    required: true,
    default: false
  },
  meta: {
    like: {
      type: Number,
      default: 0
    },
    likedBy: Array
  }
}, {
  timestamps: true,
  versionKey: false
});
module.exports = mongoose.model('blog', blogSchema);
