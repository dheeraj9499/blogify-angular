const router = require('express').Router();
const userController = require('../controllers/userController');
const { VERIFY_TOKEN } = require('../middleware/verifyUser');

router.get('/myblogs', VERIFY_TOKEN, userController.getMyBlogs);

router.get('/blog/edit/:id', userController.getBlog);

router.post('/blog/edit', VERIFY_TOKEN, userController.saveEditedBlog);

router.delete('/blog/delete/:id', VERIFY_TOKEN, userController.deleteBlog);

module.exports = router;
