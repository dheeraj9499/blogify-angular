const router = require('express').Router();
const blogController = require('../controllers/blogController');
const { VERIFY_TOKEN } = require('../middleware/verifyUser');

router.get('/:id', blogController.getBlogById);

router.post('/like', VERIFY_TOKEN, blogController.postBlogLike);

module.exports = router;
