const homeController = require('../controllers/homeController');

const router = require('express').Router();

router.get('/blogs', homeController.getBlogs);

module.exports = router;
