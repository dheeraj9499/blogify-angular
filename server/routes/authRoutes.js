const router = require('express').Router();
const authController = require('../controllers/authController');

router.post('/signup', authController.registerUser);
router.post('/login', authController.loginUser);
router.get('/getMyId', authController.getMyId);

module.exports = router;
