const router = require('express').Router();
const editorController = require('../controllers/editorController');
const { VERIFY_TOKEN } = require('../middleware/verifyUser');

router.post('/new/post', VERIFY_TOKEN, editorController.newBlog)

module.exports = router;
