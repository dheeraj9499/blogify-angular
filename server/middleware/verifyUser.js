const jwt = require('jsonwebtoken');

function VERIFY_TOKEN(req, res, next) {
  if (!req.headers.authorization) {
    return res.status(401).send('Unauthorize');
  }
  const [_, token] = req.headers.authorization.split(' ');
  if (typeof token === 'undefined' || !token) {
    return res.status(401).send('Unauthorize');
  }
  const verify = jwt.verify(token, process.env.SECRET);
  if (!verify) {
    return res.status(401).send('Unauthorize');
  }
  req.userId = verify._id;
  next();
}

module.exports.VERIFY_TOKEN = VERIFY_TOKEN;
