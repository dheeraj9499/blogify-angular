const Blog = require("../models/blogModel")

const getBlogs = async (req, res) => {

  try {
    const data = await Blog.find()
                    .where('hasPublished').equals(true)
                    .sort('-createdAt')
                    .select('_id title tags hasPublished createdAt meta')
                    .populate('userId', 'username')
    return res.json(data);
  } catch (e) {
    res.status = 404;
    return res.send("Fail");
  }
}

module.exports = {
  getBlogs
}
