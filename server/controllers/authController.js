const jwt = require('jsonwebtoken');
const { signupValidation } = require('../validations/signupValidation');
const { loginValidation } = require('../validations/loginValidation');
const User = require('../models/userModel');

/**
 * @param {*} req
 * @param {*} res
 * @verifies [USER]
 */
const registerUser = async (req, res) => {
  const data = Object.assign({}, req.body);
  const { error } = signupValidation(data);
  if (error) {
    const errorMessage = error.details[0].message;
    return res.json(errorMessage);
  }
  const emailExists = await User.findOne({ email: data.email }, 'email');
  if (emailExists) {
    res.statusCode = 403; // forbidden
    return res.json({
      message: 'Email Already exists!!'
    });
  }
  const user = new User();
  user.username = data.username;
  user.email = data.email;
  try {
    user.password = await user.generateHash(data.password);
  } catch (e) {
    res.statusCode = 403;
    return res.json({
      message: 'Something went wrong!!'
    });
  }
  user
    .save()
    .then(() => {
      const token = jwt.sign({ _id: user._id, username: user.username }, process.env.SECRET, { expiresIn: '3 days' });
      // res.cookie('accessToken', token, { maxAge: 1000 * 60 * 60 * 72, httpOnly: true });
      res.status(200).send({token, username: user.username});
    })
    .catch(() => res.json('Failed'));
}

/**
 *
 * @param {*} req [Request Object]
 * @param {*} res [Response Object]
 * @returns success (202) if user is valid else 404
 */
const loginUser = async (req, res) => {
  const data = Object.assign({}, req.body);
  const { error } = loginValidation(data);
  if (error) {
    const errorMessage = error.details[0].message;
    res.statusCode = 403; // 403: forbidden
    return res.json({
      message: errorMessage
    });
  }
  const { email, password } = data;
  User.findOne({ email }, async (err, user) => {
    if (err) {
      return res.status(404).json('Error occured');
    }
    if (!user) {
      const errorMessage = 'Email Incorrect';
      return res.status(403).json({
        message: errorMessage
      });
    }
    const isValid = await user.compareHash(password, user.password);
    if (!isValid) {
      const errorMessage = 'Password Incorrect';
      res.statusCode = 403;   // forbidden
      return res.json({
        message: errorMessage
      });
    }
    const token = jwt.sign({ _id: user._id, username: user.username }, process.env.SECRET, { expiresIn: '3 days' });
    // cookie age 3 days, can't access through the dom access only transferred via http protocol
    // res.cookie('accessToken', token, { maxAge: 1000 * 60 * 60 * 72, httpOnly: true });
    return res.status(200).send({ token, username: user.username });
  });
};

const getMyId = async (req, res) => {
  try {
    let token = req.headers.authorization.split(' ')[1];
    let payload = jwt.verify(token, process.env.SECRET);
    return res.status(201).json(payload._id);
  } catch (e) {
    return res.status(401).json({ message: 'Failed' });
  }
}


module.exports = {
  registerUser,
  loginUser,
  getMyId
}
