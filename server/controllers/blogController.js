const Blog = require('../models/blogModel');
const User = require('../models/userModel');

const getBlogById = async (req, res) => {
  try {
    const { id } = req.params;
    const data = await Blog.findById(id).populate('userId', 'username');
    return res.status(201).json(data);
  } catch (err) {
    res.send('Something went wrong!!');
  }
};

const postBlogLike = async (req, res) => {
  if (!req.body.blogId) {
    res.json({ success: false, message: 'No id was provided!'});
  } else {
    Blog.findOne({ _id: req.body.blogId }, (err, blog) => {
      if (err) {
        res.json({ success: false, message: 'Invalid Blog Id!'});
      } else {
        if (!blog) {
          res.json({ success: false, message: 'Blog Not found!' });
        } else {
          User.findOne({ _id: req.userId }, (err, user) => {
            if (err) {
              res.json({ success: false, message: 'Something went wrong!' });
            } else {
              if (user._id === blog.userId) {
                res.json({ success: false, message: 'Cannnot like your own post!' });
              } else {
                if (blog.meta.likedBy.includes(user._id)) {
                  res.json({ success: false, message: 'You already liked your post!' });
                } else {
                  blog.meta.like++;
                  blog.meta.likedBy.push(user._id);
                  blog.save((err) =>{
                    if (err) {
                      res.json({ success: false, message: 'Something went wrong!' });
                    } else {
                      res.json({ success: true, message: 'Blog Liked!' });
                    }
                  });
                }
              }
            }
          });
        }
      }
    });
  }
}

module.exports = {
  getBlogById,
  postBlogLike
}
