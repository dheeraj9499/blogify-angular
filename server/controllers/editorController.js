const { blogValidation } = require('../validations/blogValidation');
const Blog = require('../models/blogModel');

const newBlog = async (req, res) => {
  const data = Object.assign({}, req.body);
  data.userId = req.userId;
  const { error } = blogValidation(data);
  if (error) {
    const errorMessage = error.details[0].message;
    return res.status(422).json({
      message: errorMessage
    });
  }
  const blog = new Blog(data);
  await blog
    .save()
    .then(() => {
      res.status(201).json('Success');  // 201 : created
    })
    .catch(() =>{
      res.status(406).json({
        message: 'Bad request'
      });
    });
}

module.exports = {
  newBlog
}
