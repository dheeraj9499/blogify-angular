const Blog = require('../models/blogModel');

/**
 * @definition get all the blogs by userId
 * @param {*} req
 * @param {*} res
 * @returns
 */
const getMyBlogs = async (req, res) => {
  const userId = req.userId;
  try {
    const data = await Blog.find()
                    .where('userId').equals(userId)
                    .sort('-createdAt')
                    .select('_id title tags hasPublished createdAt')
                    .populate('userId', 'username')
    return res.json(data);
  } catch (e) {
    return res.status(404).json({ state: "Fail" });
  }
}

/**
 * @definition get specific blog by blog id
 * @param {*} req
 * @param {*} res
 * @returns
 */
const getBlog = async (req, res) => {
  try {
    const { id } = req.params;
    const data = await Blog.findById(id).populate('userId', 'username');
    return res.json(data);
  } catch (err) {
    return res.status(404).json({ state: "Fail" });
  }
}

/**
 *
 * @definition [first of all check that that the request is
 *              made y authorized user or not if user is
 *              authorized and has right to edit the blog
 *              then the request may proceed else return error with "Unauthorized (401)"]
 * @param {*} req
 * @param {*} res
 * @returns
 */
const saveEditedBlog = async (req, res) => {
  const userId = req.userId;
  const data = req.body;
  const id = data._id;
  try {
    const originalBlog = await Blog.findOne({ _id: id });
    if (originalBlog.userId === userId) {
      await Blog.updateOne({ _id: id }, data);
      return res.status(200).json({ state: "Success" });
    } else {
      return res.status(401).json({
        message: 'Unauthorized'
      });
    }
  } catch (e) {
    return res.status(401).json({ state: "Fail" });
  }
}

/**
 *
 * @definition [first of all check that that the request is
 *              made y authorized user or not if user is
 *              authorized and has right to edit the blog
 *              then the request may proceed else return error with "Unauthorized (401)"]
 * @param {*} req
 * @param {*} res
 * @returns
 */
const deleteBlog = async (req, res) => {
  const userId = req.userId;
  const { id } = req.params;
  try {
    const originalBlog = await Blog.findOne({ _id: id });
    if (originalBlog.userId === userId) {
      await Blog.deleteOne({ _id: id });
      return res.status(200).json({ state: "Success" });
    } else {
      return res.status(403).json({
        message: 'Unauthrized'
      });
    }
  } catch (e) {
    return res.status(401).json({ state: "Fail"});
  }
}

module.exports = {
  getMyBlogs,
  getBlog,
  saveEditedBlog,
  deleteBlog
}
