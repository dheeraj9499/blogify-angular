const JOI = require('joi');

const signupValidation = (data) => {
  const userSignupSchema = JOI.object({
    username: JOI.string()
      .min(3)
      .required(),
    email: JOI.string()
      .email()
      .required(),
    password: JOI.string()
      .min(8)
      .max(128)
      .required()
  });
  return userSignupSchema.validate(data);
};
module.exports.signupValidation = signupValidation;
