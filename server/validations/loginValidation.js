const JOI = require('joi');

const userloginSchema = JOI.object({
  email: JOI.string()
    .email()
    .required(),
  password: JOI.string()
    .min(8)
    .max(128)
    .required()
});
const loginValidation = (data) => userloginSchema.validate(data);

module.exports.loginValidation = loginValidation;
